<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class TestCacheController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 10;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
    }
################################################################################
    public function basic()
    {
        $view = new ViewModel();
        //Route
        $view->action = $this->params()->fromRoute('action', 'index');
        return $view;       
    } 
################################################################################
    public function indexAction() 
    {
        try
        {
			$data = array();
			$id = 1;
			$key_txt = "data";
			$cache = $this->maMemCache($this->cacheTime, $key_txt);
			$data = $cache->getItem($key_txt, $success);
			if( empty($data) ){
				$data = array(
					"a" => "ant",
					"b" => "bat",
					"c" => "cat555"
				);
				$cache->setItem($key_txt, $data);
				echo "No Cache!!!";
			}else{
				echo "Cache!!!";
			}
			print_r($data);
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }

    private function maMemCache($time, $namespace)
    {
        $cache = StorageFactory::factory([
											    'adapter' => [
											        'name' => 'filesystem',
											        'options' => [
											            'namespace' => $namespace,
											            'ttl' => $time,
											        ],
											    ],
											    'plugins' => [
											        // Don't throw exceptions on cache errors
											        'exception_handler' => [
											            'throw_exceptions' => true
											        ],
											        'Serializer',
											    ],
											]);
		return($cache);
	}	
}