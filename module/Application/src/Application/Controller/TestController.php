<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class TestController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 36000; // 10 hours = 36000/3600
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
    }
################################################################################
    public function basic()
    {
        $view = new ViewModel();
        //Route
        $view->action = $this->params()->fromRoute('action', 'index');
        return $view;       
    } 
################################################################################
    public function indexAction() 
    {
        try
        {
            $view = $this->basic();
			$result1 = $this->cals1(3,4);
			$view->result1 = $result1;
			
			$result2 = $this->cals2();
			$view->result2 = $result2;				
			
			$result3 = $this->cals3(5);
			$view->result3 = $result3;	
			
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }

	//3, 5, 9, 15, X  - Please create new function for finding X value
	//position 0, 1, 2, 3, 4, ...
	private function cals1($startNumber, $position)
	{
		$result = "";
		$key_txt = "result1";
		$cache = $this->maMemCache($this->cacheTime, $key_txt);
		$result = $cache->getItem($key_txt, $success);		
		
		if( empty($result) ){
			$oldResult = $startNumber;
			for($i=0;$i<=$position;$i++){
				$result = $oldResult + ($i*2);
				$oldResult = $result;
			}
			
			$cache->setItem($key_txt, $result);
		}
		return $result;
	}
	
	//(Y + 24)+(10 × 2) = 99  - Please create new function for finding Y value
	private function cals2()
	{
		$result = "";
		$key_txt = "result2";
		$cache = $this->maMemCache($this->cacheTime, $key_txt);
		$result = $cache->getItem($key_txt, $success);			
		
		if( empty($result) ){
			$x3 = 99;
			$x2 = (10*2);
			$x1 = 24;
			$result = $x3 - $x2 - $x1; //Y = 99 - (10*2) - 24 === (Y + 24)+(10 × 2) = 99
		
			$cache->setItem($key_txt, $result);
		}
		return $result;
	}

	//If 1 = 5 , 2 = 25 , 3 = 325 , 4 = 4325 Then 5 = X - Please create new function for finding X value
	private function cals3($number)
	{
		$result = "";
		$key_txt = "result3";
		$cache = $this->maMemCache($this->cacheTime, $key_txt);
		$result = $cache->getItem($key_txt, $success);			
		
		if( empty($result) ){
			$oldResult = 4;
			for($i=1;$i<=$number;$i++){
				$result = $i*pow(10,($i-1)) + $oldResult;
				$oldResult = $result;
			}
			
			$cache->setItem($key_txt, $result);
		}
		return $result;
	}

    private function maMemCache($time, $namespace)
    {
        $cache = StorageFactory::factory([
											    'adapter' => [
											        'name' => 'filesystem',
											        'options' => [
											            'namespace' => $namespace,
											            'ttl' => $time,
											        ],
											    ],
											    'plugins' => [
											        // Don't throw exceptions on cache errors
											        'exception_handler' => [
											            'throw_exceptions' => true
											        ],
											        'Serializer',
											    ],
											]);
		return($cache);
	}	
}